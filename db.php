<?php
$servername = "localhost";
$username = "root"; 
$password = ""; 
$database = "reservation_db";

// creating a new instance of the MySQLi class and establishing a connection to the MySQL database server.
$conn = new mysqli($servername, $username, $password, $database);

// This code is to print an connection error message if there is a connection problem.
if ($conn->connect_error) {
	// die() is a PHP function that is used to terminate the php script's execution and print a message before exiting.
	// In this context, die is used to terminate the php script if the server is down or credentials is wrong, then will provide a message, the message provided is from the value of the property of mysqli (connect_error)
    die("Connection failed: " . $conn->connect_error);
}
?>
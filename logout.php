<?php
session_start();

// Option A
// session_destroy() is a PHP function that terminates the session and deletes all session data.
session_destroy();


// // Option B
// // Unset all session variables
// 
// // session_unset();

// // Option C
// // Remove specific session variables 
// // This is helpful especially if you need the other session data, example in cart feature you need to reset the cart that uses session but you also need the other session data like your username and the others, therefore, you just have to unset a specific session not destroy or unset all session
// unset($_SESSION['sessionvariable']);
?>

<!DOCTYPE html>
<html>
<head>
    <title>Thank You!</title>
</head>
<body>
    <h1>Thank you for using the reservation system!</h1>
    <a href="login.php">Go back to login</a> <br>
</body>
</html>




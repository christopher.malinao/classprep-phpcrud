<?php
include 'db.php';




$search = "";
$date = "";

// Check if search parameter is provided in the URL, if there is the value will be stored to search variable
if (isset($_GET['search'])) {
    $search = $_GET['search'];
}

// Check if date parameter is provided in the URL, if there is the value will be stored to date variable
if (isset($_GET['date'])) {
    $date = $_GET['date'];
}

// SQL query to retrieve reservations based on search and date filters
// Joins the reservation table and discount table to show discounts applied in the reservation
$sql = "SELECT reservations.id, reservations.room_type, reservations.user_name, reservations.reservation_date, reservations.price, discounts.discount_name, discounts.discount_amount 
        FROM reservations 
        JOIN discounts ON reservations.discount_id = discounts.id
        WHERE (reservations.room_type LIKE '%$search%' OR reservations.user_name LIKE '%$search%' OR discounts.discount_name LIKE '%$search%')
        AND (DATE(reservations.reservation_date) = '$date' OR '$date' = '')";
$result = $conn->query($sql);


// If there is a record upon executing the sql command, a loop will run, the purpose of the loop is create list elements that contains the reservations from the record
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        echo "<li class='reservation-item' data-id='" . $row["id"] . "'>" . $row["room_type"] . " Room - Reserved by: " . $row["user_name"] . " on " . $row["reservation_date"] . " | Price: $" . $row["price"];
        if (!empty($row["discount_name"])) {
            echo " | Discounted Amount (Deducted): " . $row["discount_name"] . " (" . $row["discount_amount"] . "%)";
        }
       echo " | <a href='update.php?id=" . $row["id"] . "'>Edit</a> | <a href='delete.php?id=" . $row["id"] . "'>Delete</a></li>";

    }
} else {
    echo "<li>No reservations found for the search term: '$search' and date: '$date'</li>";
}


$conn->close();
?>

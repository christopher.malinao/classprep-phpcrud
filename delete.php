<?php
include 'db.php';

session_start();

// Check if the 'username' session variable is not set
if (!isset($_SESSION['username'])) {
   // User is not logged in, redirect to the login page
   header("Location: login.php");
   exit(); // Ensure that the script stops executing after redirection
}

echo "Navbar " . $_SESSION['username'] . "| <a href='logout.php'> Logout </a>" ;
echo "<br><hr>";

$id = $_GET['id'];

// Delete the reservation from the database
$sql = "DELETE FROM reservations WHERE id=$id";

if ($conn->query($sql) === TRUE) {
    header("Location: index.php");
} else {
    echo "Error deleting reservation: " . $conn->error;
}


$conn->close();
?>

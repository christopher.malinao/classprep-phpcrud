<?php
include 'db.php';

session_start();

// Check if the 'username' session variable is not set
if (!isset($_SESSION['username'])) {
   // User is not logged in, redirect to the login page
   header("Location: login.php");
   exit(); // Ensure that the script stops executing after redirection
}

echo "Navbar " . $_SESSION['username'] . "| <a href='logout.php'> Logout </a>" ;
echo "<br><hr>";

if (isset($_GET['id'])){
    // Display edit form
    $id = $_GET['id'];
    $sql = "SELECT * FROM reservations WHERE id=$id";
    $result = $conn->query($sql);

    if ($result && $result->num_rows > 0) {
        $row = $result->fetch_assoc();
        // Display edit form with pre-filled data
        echo "<h2>Edit Reservation</h2>" .
         "<form action='update.php' method='POST'>" .
         "<input type='hidden' name='id' value='" . $row['id'] . "'>" .
         "Room Type: <input type='text' name='room_type' value='" . $row['room_type'] . "' required><br>" .
         "User Name: <input type='text' name='user_name' value='" . $row['user_name'] . "' required><br>" .
         "Reservation Date: <input type='date' name='reservation_date' value='" . $row['reservation_date'] . "' required><br>" .
         "<!-- Include dropdown for discounts, similar to the reservation form -->" .
         "<button type='submit'>Update Reservation</button>" .
         "</form>";

    } else {
        echo "Reservation not found.";
    }
} elseif ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Handle update operation
    $id = $_POST['id'];
    $room_type = $_POST['room_type'];
    $user_name = $_POST['user_name'];
    $reservation_date = $_POST['reservation_date'];
    // ... Retrieve and process other form fields (discount, price calculation, etc.)

    // Update command to update reservation in the database
    $sql = "UPDATE reservations SET room_type='$room_type', user_name='$user_name', reservation_date='$reservation_date' WHERE id=$id";

    // Executes the update command, if successfully executed it should go to index.php
    if ($conn->query($sql) === TRUE) {
        header("Location: index.php");
    } else {
        echo "Error updating reservation: " . $conn->error;
    }
} else {
    echo "Invalid request.";
}

$conn->close();
?>

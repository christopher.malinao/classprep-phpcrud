<?php
include 'db.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $room_type = $_POST['room_type'];
    $user_name = $_POST['user_name'];
    $reservation_date = $_POST['reservation_date'];
    $discount_id = $_POST['discount'];

    // An array to define prices for different room types
    // This array uses the room type values as keys to access the price
    $room_prices = [
        'Single' => 100.00, // Price for Single Room
        'Double' => 150.00, // Price for Double Room
        'Suite' => 200.00    // Price for Suite
    ];


    # Gets the room type price of what the user selected (room_type variable contains the user selected room) using room_prices array.
    $price = $room_prices[$room_type];
    

    // Apply discount if selected
    if (!empty($discount_id)) {
        // Gets the discount amount using the discount id provided in dropdown
        $discount_sql = "SELECT discount_amount FROM discounts WHERE id = $discount_id";
        // Executes the query
        $discount_result = $conn->query($discount_sql);
        # If the discount is existing, 
        if ($discount_result->num_rows > 0) {
            # Gets the row result
            $discount_row = $discount_result->fetch_assoc();
            # Uses the row result column
            $discount_amount = $discount_row['discount_amount'];

            # price with discount computation
            $price = $price - ($price * ($discount_amount / 100));
        }
    }
    else{
        $discount_id = 0;
    }


    // Insert the reservation into the database using INSERT sql comma
    $sql = "INSERT INTO reservations (room_type, user_name, reservation_date, price, discount_id) VALUES ('$room_type', '$user_name', '$reservation_date', '$price', '$discount_id')";

    # query method returns true or false, true if the query is executed successfully, false otherwise
    # Therefore, this code executes the sql insert command if the command is successfully executed it will redirect to index.php, else it will return an error using the sql syntax and mysql error using mysqli error property.
    if ($conn->query($sql) === TRUE) {
        header("Location: index.php");
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
}

// Closing the database connection ($conn->close();) in PHP is crucial for efficient resource management, preventing connection leaks, enhancing security, and ensuring consistency. By closing the connection after database operations, you release server resources, optimizing system performance and enabling the server to handle more connections from other programs or clients. 
$conn->close();
?>
